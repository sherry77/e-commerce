import 'dart:io';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';

final String tableOrderHistory = 'order_history';
final String columnOrderHistoryId = '_id';
final String columnProductId = 'productId';
final String columnProductImageUrl = 'productImageUrl';
final String columnProductName = 'productName';
final String columnProductType = 'productType';
final String columnProductPrice = 'productPrice';
final String columnQuantity = 'quantity';

class OrderHistory {
  int orderHistoryId;
  int productId;
  String productImageUrl;
  String productName;
  String productType;
  double productPrice;
  int quantity;

  OrderHistory();

  OrderHistory.fromMap(Map<String, dynamic> map){
    orderHistoryId = map[columnOrderHistoryId];
    productId = map[columnProductId];
    productImageUrl = map[columnProductImageUrl];
    productName = map[columnProductName];
    productType = map[columnProductType];
    productPrice = map[columnProductPrice];
    quantity = map[columnQuantity];
  }

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      columnProductId: productId,
      columnProductImageUrl: productImageUrl,
      columnProductName: productName,
      columnProductType: productType,
      columnProductPrice: productPrice,
      columnQuantity: quantity
    };
    if(orderHistoryId != null){
      map[columnOrderHistoryId] = orderHistoryId;
    }
    return map;
  }
}

class OrderHistoryDatabaseHelper {
  static final _databaseName = "OrderHistoryDatabase.db";
  static final _databaseVersion = 1;

  OrderHistoryDatabaseHelper._privateConstructor();
  static final OrderHistoryDatabaseHelper instance = OrderHistoryDatabaseHelper._privateConstructor();

  static Database _database;
  Future<Database> get database async {
    if(_database != null){
      return _database;
    }
    _database = await _initDatabase();
    return _database;
  }

  _initDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);
    return await openDatabase(path, version: _databaseVersion, onCreate: _onCreate);
  }

  Future _onCreate(Database db, int version) async {
    await db.execute('''
      CREATE TABLE $tableOrderHistory (
        $columnOrderHistoryId INTEGER PRIMARY KEY,
        $columnProductId INTEGER NOT NULL,
        $columnProductImageUrl TEXT,
        $columnProductName TEXT NOT NULL,
        $columnProductType TEXT NOT NULL,
        $columnProductPrice DOUBLE NOT NULL,
        $columnQuantity INTEGER NOT NULL
      )
    ''');
  }

  Future<int> insert(OrderHistory orderHistory) async{
    Database db = await database;
    int id = await db.insert(tableOrderHistory, orderHistory.toMap());
    return id;
  }

  Future<List<OrderHistory>> getAllOrderHistoryItems() async {
    Database db = await database;
    List<Map> maps = await db.query(tableOrderHistory);
    if(maps.length > 0) {
      List<OrderHistory> orderHistoryItems = [];
      maps.forEach((map) => orderHistoryItems.add(OrderHistory.fromMap(map)));
      return orderHistoryItems;
    }
    return null;
  }

  Future<int> update(OrderHistory orderHistory) async {
    Database db = await database;
    return await db.update(tableOrderHistory, orderHistory.toMap(),
        where: '$columnOrderHistoryId = ?', whereArgs: [orderHistory.orderHistoryId]);
  }
}