import 'package:flutter/material.dart';
import 'package:e_commerce/shopping_cart_bloc.dart';
import 'package:e_commerce/shopping_cart_database_helper.dart';
import 'package:e_commerce/widget/menu_display.dart';
import 'package:e_commerce/order_history_screen.dart';
import 'package:e_commerce/user_bloc.dart';
import 'package:e_commerce/user_database_helper.dart';
import 'package:e_commerce/order_history_database_helper.dart';
import 'package:e_commerce/order_history_bloc.dart';

class ShoppingCartScreen extends StatefulWidget {

  @override
  _ShoppingCartScreenState createState() => _ShoppingCartScreenState();
}

class _ShoppingCartScreenState extends State<ShoppingCartScreen> {

  final _shoppingCartBloc = ShoppingCartBloc();
  final _userBloc = UserBloc();
  final _orderHistoryBloc = OrderHistoryBloc();

  //List<int> quantity = List<int>();

  bool _selectAllChecked = false;

  int discount = 0;

  double balance = 0;

  //double priceToPay = 0;

  bool oneChecked = false;

  @override
  void initState(){
    super.initState();
    _shoppingCartBloc.getAllShoppingCartItems();
    _shoppingCartBloc.initializeTotalPrice();
    _userBloc.getUser();
    _orderHistoryBloc.getAllOrderHistoryItems();
  }

  @override
  void dispose(){
    super.dispose();
    _shoppingCartBloc.dispose();
    _userBloc.dispose();
    _orderHistoryBloc.dispose();
  }

  bool checkFalse(List<bool> values){
    bool check = true;
    for(int i = 0; i < values.length; i++){
      if(!values[i]){
        check = false;
        break;
      }
    }
    return check;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Shopping Cart"),
      ),
      body: StreamBuilder<List<ShoppingCart>>(
        stream: _shoppingCartBloc.shoppingCart$,
        builder: (context, shoppingCartSnapshot) {

          if(shoppingCartSnapshot.hasData && shoppingCartSnapshot.data.length > 0) {

            _shoppingCartBloc.initializeCheckedList(shoppingCartSnapshot.data.length);

            /*for(int i=0; i<shoppingCartSnapshot.data.length; i++){
              quantity.insert(i, shoppingCartSnapshot.data[i].quantity);
            }*/

            _shoppingCartBloc.initializeProductQuantity(shoppingCartSnapshot.data);

            return StatefulBuilder(
              builder: (BuildContext context, StateSetter setState){
                return Transform.scale(
                  scale: 1.0,
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        CheckboxListTile(
                            title: Text('Select All'),
                            value: _selectAllChecked,
                            onChanged: (bool isChecked){
                              setState((){
                                _selectAllChecked = isChecked;
                                if(_selectAllChecked){
                                  //_all = true;
                                  _shoppingCartBloc.addAllToSelectedItems(shoppingCartSnapshot.data);
                                } else{
                                  //_all = false;
                                  _shoppingCartBloc.removeAllFromSelectedItems();
                                }
                              });
                            }
                        ),
                        Container(
                          margin: EdgeInsets.all(20.0),
                          child: ListView.builder(
                              scrollDirection: Axis.vertical,
                              shrinkWrap: true,
                              physics: ScrollPhysics(),
                              itemCount: shoppingCartSnapshot.data.length,
                              itemBuilder: (BuildContext ctx, index) {
                                return Container(
                                  margin: EdgeInsets.symmetric(vertical: 20.0),
                                  padding: EdgeInsets.symmetric(vertical: 20.0),
                                  alignment: Alignment.center,
                                  child: Stack(
                                    children: [
                                      StreamBuilder<List<bool>>(
                                        stream: _shoppingCartBloc.checkedShoppingCartItems$,
                                        builder: (context, checkedSnapshot){

                                          if(!checkedSnapshot.hasData){

                                            return Center(
                                              child: CircularProgressIndicator(),
                                            );
                                          }

                                          return Row(
                                            children: [
                                              Expanded(
                                                  flex:1,
                                                  child: Theme(
                                                      data: ThemeData(unselectedWidgetColor: Colors.white),
                                                      //color: Colors.white,
                                                      child: Checkbox(
                                                          checkColor: Colors.black,
                                                          activeColor: Colors.white,
                                                          value: checkedSnapshot.data[index],
                                                          onChanged: (bool check) {
                                                            setState((){
                                                              if(!check){
                                                                _selectAllChecked = false;
                                                                //if(_all) _all = false;
                                                              }
                                                            });
                                                            if(check){
                                                              _shoppingCartBloc.addToSelectedItems(shoppingCartSnapshot.data[index], index);
                                                            } else{
                                                              _shoppingCartBloc.removeFromSelectedItems(shoppingCartSnapshot.data[index], index);
                                                            }
                                                            setState((){
                                                              _selectAllChecked = checkFalse(checkedSnapshot.data);
                                                            });
                                                          }
                                                      ))
                                              ),
                                              Expanded(
                                                flex: 3,
                                                child: Column(
                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                  crossAxisAlignment: CrossAxisAlignment.center,
                                                  children: [
                                                    Icon(Icons.shopping_cart),
                                                    Text(shoppingCartSnapshot.data[index].productName),
                                                    Text("RM${shoppingCartSnapshot.data[index].productPrice}")
                                                  ],
                                                ),
                                              ),
                                              StreamBuilder<List<int>>(
                                                  stream: _shoppingCartBloc.productQuantity$,
                                                  builder: (context, productQuantitySnapshot){
                                                    if(!productQuantitySnapshot.hasData){
                                                      return Center(
                                                        child: CircularProgressIndicator(),
                                                      );
                                                    }
                                                    return Expanded(
                                                      flex: 3,
                                                      child: Row(
                                                        children: [
                                                          IconButton(
                                                              icon: Icon(
                                                                Icons.minimize_sharp,
                                                                color: Colors.white, size: 20.0,
                                                              ),
                                                              onPressed: () {
                                                                /*setState((){
                                                            if(productQuantitySnapshot.data[index] > 1) {
                                                              productQuantitySnapshot.data[index]--;
                                                              ShoppingCart item = shoppingCartSnapshot.data[index];
                                                              _shoppingCartBloc.updateShoppingCart(item, productQuantitySnapshot.data[index]);
                                                              //price -= item.productPrice;
                                                              _shoppingCartBloc.reduceTotalPrice(shoppingCartSnapshot.data[index].productPrice);
                                                            }
                                                          });*/
                                                                if(productQuantitySnapshot.data[index] > 1) {
                                                                  //productQuantitySnapshot.data[index]--;
                                                                  _shoppingCartBloc.reduceQuantity(index);
                                                                  ShoppingCart item = shoppingCartSnapshot.data[index];
                                                                  _shoppingCartBloc.updateShoppingCart(item, productQuantitySnapshot.data[index]);
                                                                  //price -= item.productPrice;
                                                                  if(checkedSnapshot.data[index]){
                                                                    _shoppingCartBloc.reduceTotalPrice(shoppingCartSnapshot.data[index].productPrice);
                                                                  }
                                                                }
                                                              }),
                                                          Text('${productQuantitySnapshot.data[index]}', style: TextStyle(color: Colors.white),),
                                                          IconButton(
                                                              icon: Icon(
                                                                Icons.add,
                                                                color: Colors.white, size: 20.0,
                                                              ),
                                                              onPressed: () {
                                                                /*setState(() {
                                                            if(productQuantitySnapshot.data[index] < 10){
                                                              productQuantitySnapshot.data[index] += 1;
                                                              print('Clicked! ${productQuantitySnapshot.data[index]}');
                                                              ShoppingCart item = shoppingCartSnapshot.data[index];
                                                              _shoppingCartBloc.updateShoppingCart(item, productQuantitySnapshot.data[index]);
                                                            }
                                                            _shoppingCartBloc.addToTotalPrice(shoppingCartSnapshot.data[index].productPrice);
                                                          });*/
                                                                //productQuantitySnapshot.data[index] += 1;
                                                                _shoppingCartBloc.addQuantity(index);
                                                                print('Clicked! ${productQuantitySnapshot.data[index]}');
                                                                ShoppingCart item = shoppingCartSnapshot.data[index];
                                                                _shoppingCartBloc.updateShoppingCart(item, productQuantitySnapshot.data[index]);
                                                                if(checkedSnapshot.data[index]){
                                                                  _shoppingCartBloc.addToTotalPrice(shoppingCartSnapshot.data[index].productPrice);
                                                                }
                                                              }),
                                                          Expanded(
                                                            flex:1,
                                                            child: IconButton(
                                                              icon: Icon(Icons.delete_rounded),
                                                              iconSize: 20.0,
                                                              onPressed: (){
                                                                //remove from list
                                                                _shoppingCartBloc.deleteShoppingCart(shoppingCartSnapshot.data, shoppingCartSnapshot.data[index]);
                                                              },
                                                            ),
                                                          )
                                                        ],
                                                      ),
                                                    );
                                                  }
                                              ),
                                            ],
                                          );
                                        }
                                      )
                                    ],
                                  ),
                                  decoration: BoxDecoration(
                                      color: Colors.blue,
                                      borderRadius: BorderRadius.circular(15)),
                                );
                              }),
                        )
                      ],
                    ),
                  ),
                );
              },
            );
          }
          return Center(child: Text("Shopping cart is empty"));
        },
      ),
      drawer: MenuDisplay(""),
      bottomNavigationBar: BottomAppBar(
        child: Container(
          margin: EdgeInsets.all(20.0),
          width: MediaQuery.of(context).size.width,
          child: StreamBuilder<List<ShoppingCart>>(
            stream: _shoppingCartBloc.selectedItems$,
            builder: (context, selectedItemsSnapshot){
              if(selectedItemsSnapshot.hasData && selectedItemsSnapshot.data.length > 0){
                for(int i = 0; i < selectedItemsSnapshot.data.length; i++){
                  //_shoppingCartBloc.addToTotalPrice(selectedItemsSnapshot.data[i].productPrice * selectedItemsSnapshot.data[i].quantity);
                  //price += selectedItemsSnapshot.data[i].productPrice * selectedItemsSnapshot.data[i].quantity;
                }
              }
              return StreamBuilder<double>(
                initialData: 0,
                stream: _shoppingCartBloc.totalPrice$,
                builder: (context, priceSnapshot){
                  return Wrap(
                    children: [
                      Text('Total price: RM${priceSnapshot.data}'),
                      SizedBox(height: 20.0,),
                      StreamBuilder<User>(
                        stream: _userBloc.user$,
                        builder: (context, userSnapshot){
                          if(!userSnapshot.hasData){
                            return Center(
                              child: CircularProgressIndicator(),
                            );
                          }

                          if(userSnapshot.data.totalPoints > 0){
                            discount = (userSnapshot.data.totalPoints/1000).toInt();
                          }

                          double totalDiscount = userSnapshot.data.totalPoints/1000;
                          if(totalDiscount > priceSnapshot.data){
                            _shoppingCartBloc.calculateTotalToPay(0);
                            balance = (totalDiscount - priceSnapshot.data) * 1000;
                          } else{
                            //priceToPay = priceSnapshot.data - totalDiscount;
                            _shoppingCartBloc.calculateTotalToPay(priceSnapshot.data - totalDiscount);
                            balance = 0;
                          }

                          return Row(
                            children: [
                              Expanded(
                                flex: 1,
                                child: Text("Total points: ${userSnapshot.data.totalPoints}"),
                              ),
                              Expanded(
                                flex: 1,
                                child: Text("Discount: RM${userSnapshot.data.totalPoints/1000} off"),
                              )
                            ],
                          );
                        },
                      ),
                      SizedBox(height: 20.0,),
                      StreamBuilder<double>(
                        stream: _shoppingCartBloc.totalToPay$,
                        builder: (context, totalToPaySnapshot){
                          return Text("Total to pay: ${totalToPaySnapshot.data}");
                        },
                      ),
                      SizedBox(height: 20.0,),
                      StreamBuilder<List<OrderHistory>>(
                        stream: _shoppingCartBloc.orderHistoryList$,
                        builder: (context, orderHistoryListSnapshot){
                          return StreamBuilder<double>(
                              initialData: 0,
                              stream: _shoppingCartBloc.totalPrice$,
                              builder: (context, priceSnapshot){
                                return StreamBuilder<List<bool>>(
                                  stream: _shoppingCartBloc.checkedShoppingCartItems$,
                                  builder: (context, checkedSnapshot){
                                    if(checkedSnapshot.hasData){
                                      for(int i = 0; i < checkedSnapshot.data.length; i++){
                                        if(checkedSnapshot.data[i]){
                                          oneChecked = true;
                                          break;
                                        }
                                      }
                                    }
                                    return RaisedButton(
                                      color: oneChecked ? Colors.blue : Colors.white10,
                                      shape: StadiumBorder(),
                                      onPressed: () {
                                        if(oneChecked){
                                          int pointsAdded = (priceSnapshot.data * 100).toInt();
                                          print("Point: $pointsAdded");
                                          //_userBloc.updateUser(balance.toInt());
                                          if(pointsAdded > 0){
                                            _userBloc.updateUser(balance.toInt() + pointsAdded);
                                          }
                                          _shoppingCartBloc.insertToOrderHistory(selectedItemsSnapshot.data);
                                          _shoppingCartBloc.removeCartItemsAfterCheckout(selectedItemsSnapshot.data);
                                          _orderHistoryBloc.getAllOrderHistoryItems();
                                          OrderHistory();
                                          Navigator.push(
                                            context,
                                            MaterialPageRoute(builder: (context) {
                                              return OrderHistoryScreen();
                                            }),
                                          );
                                          /*Navigator.push(
                                            context,
                                            MaterialPageRoute(builder: (context) => OrderHistoryScreen()),
                                          );*/
                                        }
                                      },
                                      child: Container(
                                          width: MediaQuery.of(context).size.width,
                                          padding: EdgeInsets.all(10.0),
                                          child: Text("Check Out", textAlign: TextAlign.center, style: TextStyle(color: Colors.white, fontSize: 20.0),)
                                      ),
                                    );
                                  }
                                );
                              }
                          );
                        }
                      )
                    ],
                  );
                }
              );
            },
          ),
        ),
      ),
    );
  }
}
