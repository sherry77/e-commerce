import 'package:flutter/material.dart';
import 'package:e_commerce/wishlist_screen.dart';
import 'package:e_commerce/products_screen.dart';
import 'package:e_commerce/order_history_screen.dart';

class MenuDisplay extends StatelessWidget {
  final String screenTitle;

  MenuDisplay(this.screenTitle);

  @override
  Widget build(BuildContext context){
    return Drawer(
      child: ListView(
        children: [
          ListTile(
            title: Text('Products', style: TextStyle(color: this.screenTitle == ScreenTitle.products ? Colors.blueAccent : Colors.black),),
            onTap: (){
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => ProductsScreen()),
              );
            },
          ),
          ListTile(
            title: Text('Wishlist', style: TextStyle(color: this.screenTitle == ScreenTitle.wishlist ? Colors.blueAccent : Colors.black),),
            onTap: (){
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => WishListScreen()),
              );
            },
          ),
          ListTile(
            title: Text('Order History', style: TextStyle(color: this.screenTitle == ScreenTitle.orderHistory ? Colors.blueAccent : Colors.black),),
            onTap: (){
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => OrderHistoryScreen()),
              );
            },
          ),
        ],
      ),
    );
  }
}

class ScreenTitle {
  static String products = "Products Screen";
  static String wishlist = "Wishlist Screen";
  static String orderHistory = "Order History Screen";
}