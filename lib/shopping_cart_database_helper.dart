import 'dart:io';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';

final String tableShoppingCart = 'shopping_cart';
final String columnShoppingCartId = '_id';
final String columnProductId = 'productId';
final String columnProductImageUrl = 'productImageUrl';
final String columnProductName = 'productName';
final String columnProductType = 'productType';
final String columnProductPrice = 'productPrice';
final String columnQuantity = 'quantity';

class ShoppingCart {
  int shoppingCartId;
  int productId;
  String productImageUrl;
  String productName;
  String productType;
  double productPrice;
  int quantity;

  ShoppingCart();

  ShoppingCart.fromMap(Map<String, dynamic> map){
    shoppingCartId = map[columnShoppingCartId];
    productId = map[columnProductId];
    productImageUrl = map[columnProductImageUrl];
    productName = map[columnProductName];
    productType = map[columnProductType];
    productPrice = map[columnProductPrice];
    quantity = map[columnQuantity];
  }

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      columnProductId: productId,
      columnProductImageUrl: productImageUrl,
      columnProductName: productName,
      columnProductType: productType,
      columnProductPrice: productPrice,
      columnQuantity: quantity
    };
    if(shoppingCartId != null){
      map[columnShoppingCartId] = shoppingCartId;
    }
    return map;
  }
}

class ShoppingCartDatabaseHelper {
  static final _databaseName = "ShoppingCartDatabase.db";
  static final _databaseVersion = 1;

  ShoppingCartDatabaseHelper._privateConstructor();
  static final ShoppingCartDatabaseHelper instance = ShoppingCartDatabaseHelper._privateConstructor();

  static Database _database;
  Future<Database> get database async {
    if(_database != null){
      return _database;
    }
    _database = await _initDatabase();
    return _database;
  }

  _initDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);
    return await openDatabase(path, version: _databaseVersion, onCreate: _onCreate);
  }

  Future _onCreate(Database db, int version) async {
    await db.execute('''
      CREATE TABLE $tableShoppingCart (
        $columnShoppingCartId INTEGER PRIMARY KEY,
        $columnProductId INTEGER NOT NULL,
        $columnProductImageUrl TEXT,
        $columnProductName TEXT NOT NULL,
        $columnProductType TEXT NOT NULL,
        $columnProductPrice DOUBLE NOT NULL,
        $columnQuantity INTEGER NOT NULL
      )
    ''');
  }

  Future<int> insert(ShoppingCart shoppingCart) async{
    Database db = await database;
    int id = await db.insert(tableShoppingCart, shoppingCart.toMap());
    return id;
  }

  Future<List<ShoppingCart>> getAllShoppingCartItems() async {
    Database db = await database;
    List<Map> maps = await db.query(tableShoppingCart);
    if(maps.length > 0) {
      List<ShoppingCart> shoppingCartItems = [];
      maps.forEach((map) => shoppingCartItems.add(ShoppingCart.fromMap(map)));
      return shoppingCartItems;
    }
    return null;
  }

  Future<int> update(ShoppingCart cart) async {
    Database db = await database;
    return await db.update(tableShoppingCart, cart.toMap(),
        where: '$columnShoppingCartId = ?', whereArgs: [cart.shoppingCartId]);
  }

  Future<int> deleteShoppingCartItem(int id) async {
    Database db = await database;
    return await db.delete(tableShoppingCart, where: '$columnShoppingCartId = ?', whereArgs: [id]);
  }
}