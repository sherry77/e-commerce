import 'package:e_commerce/products_screen.dart';
import 'package:flutter/material.dart';
import 'package:e_commerce/user_bloc.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'E-Commerce',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'E-Commerce'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  final _userBloc = UserBloc();

  TextEditingController _usernameController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();

  bool error = false;

  @override
  void initState(){
    super.initState();
    _userBloc.createUser();
  }

  @override
  void dispose(){
    super.dispose();
    _userBloc.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Container(
        margin: EdgeInsets.all(30.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              'Username:',
            ),
            Container(
              margin: EdgeInsets.symmetric(vertical: 20.0),
              child: TextField(
                  controller: _usernameController,
                  decoration: InputDecoration(
                    hintText: 'Key in your username...',
                    hintStyle: TextStyle(color: Colors.grey),
                    filled: true,
                    fillColor: Colors.white70,
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(12.0)),
                      borderSide: BorderSide(color: Colors.blue, width: 2),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      borderSide: BorderSide(color: Colors.blue),
                    ),
                  )
              ),
            ),
            Text(
              'Password:',
            ),
            Container(
              margin: EdgeInsets.symmetric(vertical: 20.0),
              child: TextField(
                  controller: _passwordController,
                  decoration: InputDecoration(
                    hintText: 'Key in your password...',
                    hintStyle: TextStyle(color: Colors.grey),
                    filled: true,
                    fillColor: Colors.white70,
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(12.0)),
                      borderSide: BorderSide(color: Colors.blue, width: 2),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      borderSide: BorderSide(color: Colors.blue),
                    ),
                  )
              ),
            ),
            Container(
              margin: EdgeInsets.all(20.0),
              width: MediaQuery.of(context).size.width,
              child: RaisedButton(
                  color: Colors.blue,
                  shape: StadiumBorder(),
                  onPressed: () {
                    if((_usernameController.text != null && _usernameController.text != "") && (_passwordController.text != null && _passwordController.text != "")){
                      if(_usernameController.text == "Sherry" && _passwordController.text == "Sherry@9000"){
                        setState(() {
                          error = false;
                        });
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => ProductsScreen()),
                        );
                      } else{
                        setState(() {
                          error = true;
                        });
                      }
                    } else{
                      setState(() {
                        error = true;
                      });
                    }
                  },
                  child: Padding(
                      padding: EdgeInsets.all(10.0),
                      child: Text("Login", style: TextStyle(color: Colors.white, fontSize: 20.0),)
                  )
              ),
            ),
            //SizedBox(height: 20.0,),
            error ? Text('Wrong username or password! Please try again.', style: TextStyle(color: Colors.red, fontSize: 15.0),) : SizedBox.shrink()
          ],
        ),
      ),
    );
  }
}
