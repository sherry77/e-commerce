import 'package:rxdart/rxdart.dart';
import 'package:e_commerce/order_history_database_helper.dart';

class OrderHistoryBloc {

  final _orderHistorySubject = BehaviorSubject<List<OrderHistory>>();
  BehaviorSubject<List<OrderHistory>> get orderHistory$ => _orderHistorySubject.stream;

  getAllOrderHistoryItems() async{
    List<OrderHistory> orderHistoryList = List<OrderHistory>();
    OrderHistoryDatabaseHelper helper = OrderHistoryDatabaseHelper.instance;
    final orderHistoryItems = await helper.getAllOrderHistoryItems();
    if(orderHistoryItems != null) {
      orderHistoryItems.forEach((orderHistoryItems) {
        orderHistoryList.add(orderHistoryItems);
      });
      _orderHistorySubject.sink.add(orderHistoryList);
    }
    print("OHL: ${orderHistoryList.length}");
  }

  updateOrderHistoryList(List<OrderHistory> list){
    _orderHistorySubject.sink.add(list);
  }

  void dispose(){
    _orderHistorySubject.close();
  }
}