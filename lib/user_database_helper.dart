import 'dart:io';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';

final String tableUser = 'user';
final String columnUserId = '_id';
final String columnUsername = 'username';
final String columnPassword = 'password';
final String columnTotalPoints = 'totalPoints';

class User {
  int userId;
  String username;
  String password;
  int totalPoints;

  User();

  User.fromMap(Map<String, dynamic> map){
    userId = map[columnUserId];
    username = map[columnUsername];
    password = map[columnPassword];
    totalPoints = map[columnTotalPoints];
  }

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      columnUsername: username,
      columnPassword: password,
      columnTotalPoints: totalPoints,
    };
    if(userId != null){
      map[columnUserId] = userId;
    }
    return map;
  }
}

class UserDatabaseHelper {
  static final _databaseName = "UserDatabase.db";
  static final _databaseVersion = 1;

  UserDatabaseHelper._privateConstructor();
  static final UserDatabaseHelper instance = UserDatabaseHelper._privateConstructor();

  static Database _database;
  Future<Database> get database async {
    if(_database != null){
      return _database;
    }
    _database = await _initDatabase();
    return _database;
  }

  _initDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);
    return await openDatabase(path, version: _databaseVersion, onCreate: _onCreate);
  }

  Future _onCreate(Database db, int version) async {
    await db.execute('''
      CREATE TABLE $tableUser (
        $columnUserId INTEGER PRIMARY KEY,
        $columnUsername TEXT NOT NULL,
        $columnPassword TEXT NOT NULL,
        $columnTotalPoints INTEGER NOT NULL
      )
    ''');
  }

  Future<int> insert(User user) async{
    Database db = await database;
    int id = await db.insert(tableUser, user.toMap());
    return id;
  }

  Future<User> getUser() async {
    Database db = await database;
    List<Map> map = await db.query(
        tableUser,
        columns: [columnUserId, columnUsername, columnPassword, columnTotalPoints],
        where: '$columnUsername = ? AND $columnPassword = ?',
        whereArgs: ['Sherry', 'Sherry@9000']
    );
    if(map.length > 0) {
      return User.fromMap(map.first);
    }
    return null;
  }

  Future<int> update(User user) async {
    Database db = await database;
    return await db.update(tableUser, user.toMap(),
        where: '$columnUserId = ?', whereArgs: [user.userId]);
  }
}