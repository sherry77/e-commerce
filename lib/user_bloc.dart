import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';
import 'package:e_commerce/user_database_helper.dart';

class UserBloc {

  final _userSubject = BehaviorSubject<User>();
  BehaviorSubject<User> get user$ => _userSubject.stream;

  createUser() async{

    UserDatabaseHelper helper = UserDatabaseHelper.instance;

    User user = await helper.getUser();

    if(user == null){

      User newUser = User();
      newUser.username = "Sherry";
      newUser.password = "Sherry@9000";
      newUser.totalPoints = 0;
      user = newUser;
      await helper.insert(user);
    }

    _userSubject.sink.add(user);
  }

  getUser() async{
    User user = User();
    UserDatabaseHelper helper = UserDatabaseHelper.instance;
    user = await helper.getUser();
    _userSubject.sink.add(user);

    print("${user.username}, ${user.totalPoints}");
  }

  updateUser(int points) async{
    User existingUser = User();
    UserDatabaseHelper helper = UserDatabaseHelper.instance;
    existingUser = await helper.getUser();

    User user = User();
    user.userId = existingUser.userId;
    user.username = existingUser.username;
    user.password = existingUser.password;
    user.totalPoints = points;
    await helper.update(user);

    print("Points added: $points, Points update: ${user.totalPoints}");
  }

  void dispose(){
    _userSubject.close();
  }
}
