import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';
import 'package:e_commerce/shopping_cart_database_helper.dart';
import 'package:e_commerce/wishlist_database_helper.dart';
import 'package:e_commerce/product_database_helper.dart';

class ProductType{
  static String all = "All food and drinks";
  static String food = "Food";
  static String drinks = "Drinks";
}

class ProductsBloc{

  final _productsSubject = BehaviorSubject<List<Product>>();
  BehaviorSubject<List<Product>> get products$ => _productsSubject.stream;

  final _foodSubject = BehaviorSubject<List<Product>>();
  BehaviorSubject<List<Product>> get foodProduct$ => _foodSubject.stream;

  final _drinksSubject = BehaviorSubject<List<Product>>();
  BehaviorSubject<List<Product>> get drinkProduct$ => _drinksSubject.stream;

  final _shoppingCartSubject = BehaviorSubject<List<Product>>();
  BehaviorSubject<List<Product>> get shoppingCart$ => _shoppingCartSubject.stream;

  final _wishlistSubject = BehaviorSubject<List<Product>>();
  BehaviorSubject<List<Product>> get wishlist$ => _wishlistSubject.stream;

  final _productTypeSubject = BehaviorSubject<int>.seeded(0);
  BehaviorSubject<int> get productType$ => _productTypeSubject.stream;

  List<Product> wishlistList = List<Product>();

  List<Product> shoppingCartList = List<Product>();

  createProducts() async{

    ProductDatabaseHelper helper = ProductDatabaseHelper.instance;

    List<Product> allProducts = await helper.getAllProducts();

    if(allProducts == null || allProducts.length == 0){
      List<Product> products = List<Product>();

      for(int i=0; i<10; i++){
        String productName = "Product ${i+1}";
        String productType = ProductType.food;
        double productPrice = 30.00;
        if(i.isOdd){
          productType = ProductType.drinks;
          productPrice = 15.00;
        }
        //Product product = Product(i+1, null, productName, productType, productPrice);
        //products.add(product);
        Product product = Product();
        product.productId = i + 1;
        product.productImageUrl = "";
        product.productName = productName;
        product.productType = productType;
        product.productPrice = productPrice;
        product.productQuantity = 10;
        int id = await helper.insert(product);
        products.add(product);
      }

      _productsSubject.sink.add(products);
    }
  }

  getAllProducts() async{
    List<Product> products = List<Product>();
    //_productsSubject.sink.add(products);
    ProductDatabaseHelper helper = ProductDatabaseHelper.instance;
    final productList = await helper.getAllProducts();
    if(productList != null) {
      productList.forEach((productList) {
        products.add(productList);
      });
      print("Product List: ${productList}");
      print("products in list view: ${products}");
      _productsSubject.sink.add(products);
    }
  }

  getFoodProduct() async{
    List<Product> products = List<Product>();
    print("products in list view: ${products.length}");
    _productsSubject.add(products);
    if(products.length == 0){
      ProductDatabaseHelper helper = ProductDatabaseHelper.instance;
      final productList = await helper.getProducts(ProductType.food);
      if(productList != null) {
        productList.forEach((productList) {
          products.add(productList);
        });
        _foodSubject.add(products);
      }
    }
  }

  getDrinksProduct() async{
    List<Product> products = List<Product>();
    _productsSubject.add(products);
    if(products.length == 0){
      ProductDatabaseHelper helper = ProductDatabaseHelper.instance;
      final productList = await helper.getProducts(ProductType.drinks);
      if(productList != null) {
        productList.forEach((productList) {
          products.add(productList);
        });
        _drinksSubject.sink.add(products);
      }
    }
  }

  /*getSpecificProducts(String type) async {
    List<Product> foodProduct = List<Product>();
    if(type == ProductType.food){

    }
    List<Product> products = List<Product>();
    _productsSubject.add(products);
    if(products.length == 0){
      ProductDatabaseHelper helper = ProductDatabaseHelper.instance;
      final productList = await helper.getProducts(type);
      if(productList != null) {
        productList.forEach((productList) {
          products.add(productList);
        });
        _productsSubject.sink.add(products);
      }
    }
  }*/

  productTypeChanged(int type) {
    _productTypeSubject.sink.add(type);
    if(type == 1){
      getFoodProduct();
    } else if(type == 2){
      getDrinksProduct();
    } else {
      getAllProducts();
    }
  }

  /*addProductToShoppingCart(int productId){
    if(products.length > 0){
      for(int i = 0; i < products.length; i++){
        if(products[i].productId == productId){
          Product shoppingCartItem = Product(products[i].productId, products[i].productImageUrl, products[i].productName, products[i].productType, products[i].productPrice);
          shoppingCartList.add(shoppingCartItem);
          _shoppingCartSubject.sink.add(shoppingCartList);
          break;
        }
      }
    }
  }

  addProductToWishList(int productId){
    if(products.length > 0){
      for(int i = 0; i <products.length; i++){
        if(products[i].productId == productId){
          Product product = products[i];
          Product  wishlistItem = Product(product.productId, product.productImageUrl, product.productName, product.productType, product.productPrice);
          wishlistList.add(wishlistItem);
          _wishlistSubject.sink.add(wishlistList);
        }
      }
    }
  }*/

  /*insertToShoppingCart(Product product) async{
    ShoppingCart shoppingCart = ShoppingCart();
    shoppingCart.productId = product.productId;
    shoppingCart.productImageUrl = product.productImageUrl;
    shoppingCart.productName = product.productName;
    shoppingCart.productType = product.productType;
    shoppingCart.productPrice = product.productPrice;
    //temporary set to 1
    shoppingCart.quantity = 1;

    ShoppingCartDatabaseHelper helper = ShoppingCartDatabaseHelper.instance;
    int id = await helper.insert(shoppingCart);
  }*/

  insertToShoppingCart(Product product) async{
    ShoppingCartDatabaseHelper helper = ShoppingCartDatabaseHelper.instance;
    List<ShoppingCart> cartItems = await helper.getAllShoppingCartItems();
    bool cartExist = false;
    if(cartItems != null && cartItems.length > 0){
      for(int i = 0; i < cartItems.length; i++){
        if(cartItems[i].productId == product.productId){
          cartExist = true;
          updateShoppingCart(cartItems[i], cartItems[i].quantity + 1);
          break;
        }
      }
    }
    if(!cartExist){
      ShoppingCart shoppingCart = ShoppingCart();
      shoppingCart.productId = product.productId;
      shoppingCart.productImageUrl = product.productImageUrl;
      shoppingCart.productName = product.productName;
      shoppingCart.productType = product.productType;
      shoppingCart.productPrice = product.productPrice;
      shoppingCart.quantity = 1;
      await helper.insert(shoppingCart);
    }
  }

  updateShoppingCart(ShoppingCart item, int quantity) async{
    print("Update quantity: ${quantity}");
    ShoppingCart cart = ShoppingCart();
    cart.shoppingCartId = item.shoppingCartId;
    cart.productId = item.productId;
    cart.productImageUrl = item.productImageUrl;
    cart.productName = item.productName;
    cart.productType = item.productType;
    cart.productPrice = item.productPrice;
    cart.quantity = quantity;
    final helper = ShoppingCartDatabaseHelper.instance;
    await helper.update(cart);
  }

  insertToWishlist(Product product) async{
    WishlistDatabaseHelper helper = WishlistDatabaseHelper.instance;
    List<Wishlist> wishlistItems = await helper.getAllWishlistItems();
    bool wishlistExist = false;
    if(wishlistItems != null && wishlistItems.length > 0){
      for(int i = 0; i < wishlistItems.length; i++){
        if(wishlistItems[i].productId == product.productId){
          wishlistExist = true;
          break;
        }
      }
    }
    if(!wishlistExist){
      Wishlist wishlist = Wishlist();
      wishlist.productId = product.productId;
      wishlist.productImageUrl = product.productImageUrl;
      wishlist.productName = product.productName;
      wishlist.productType = product.productType;
      wishlist.productPrice = product.productPrice;
      await helper.insert(wishlist);
    }
  }

  void dispose(){
    _productsSubject.close();
    _shoppingCartSubject.close();
    _wishlistSubject.close();
    _productTypeSubject.close();
    _foodSubject.close();
    _drinksSubject.close();
  }

}