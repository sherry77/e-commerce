import 'package:rxdart/rxdart.dart';
import 'package:e_commerce/wishlist_database_helper.dart';

class WishlistBloc {

  final _wishlistSubject = BehaviorSubject<List<Wishlist>>();
  BehaviorSubject<List<Wishlist>> get wishlist$ => _wishlistSubject.stream;

  getAllWishlistItems() async{
    List<Wishlist> wishlist = List<Wishlist>();
    WishlistDatabaseHelper helper = WishlistDatabaseHelper.instance;
    final wishlistItems = await helper.getAllWishlistItems();
    if(wishlistItems != null) {
      wishlistItems.forEach((wishlistItems) {
        wishlist.add(wishlistItems);
      });
      _wishlistSubject.sink.add(wishlist);
    }
  }

  void dispose(){
    _wishlistSubject.close();
  }
}