import 'package:e_commerce/shopping_cart_bloc.dart';
import 'package:flutter/material.dart';
import 'package:e_commerce/products_bloc.dart';
import 'package:e_commerce/shopping_cart_screen.dart';
import 'package:e_commerce/widget/menu_display.dart';
import 'package:e_commerce/shopping_cart_database_helper.dart';
import 'package:e_commerce/product_database_helper.dart';

class DrinksProductsScreen extends StatefulWidget {

  DrinksProductsScreen(this.productList);

  List<Product> productList;

  @override
  _DrinksProductsScreenState createState() => _DrinksProductsScreenState();
}

class _DrinksProductsScreenState extends State<DrinksProductsScreen> {

  final _productsBloc = ProductsBloc();
  final _shoppingCartBloc = ShoppingCartBloc();

  bool itemExist = false;

  //int radioValue = 0;

  @override
  void initState(){
    super.initState();
    _shoppingCartBloc.getAllShoppingCartItems();
  }

  @override
  void dispose(){
    super.dispose();
    _productsBloc.dispose();
    _shoppingCartBloc.dispose();
  }

  @override
  Widget build(BuildContext context) {
    List<Product> productList = widget.productList;
    return Container(
      margin: EdgeInsets.all(20.0),
      child: GridView.builder(
          scrollDirection: Axis.vertical,
          shrinkWrap: true,
          physics: ScrollPhysics(),
          gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
              maxCrossAxisExtent: 200,
              childAspectRatio: 3 / 2,
              crossAxisSpacing: 20,
              mainAxisSpacing: 20),
          itemCount: productList.length,
          itemBuilder: (BuildContext ctx, index) {
            return Stack(
              children: [
                Container(
                  alignment: Alignment.center,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(Icons.shopping_cart),
                      Text(productList[index].productName),
                      Text("RM${productList[index].productPrice}")
                    ],
                  ),
                  decoration: BoxDecoration(
                      color: Colors.blue,
                      borderRadius: BorderRadius.circular(15)),
                ),
                Container(
                  margin: EdgeInsets.all(10.0),
                  child: Align(
                      alignment: Alignment.bottomRight,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          StreamBuilder<List<ShoppingCart>>(
                            stream: _shoppingCartBloc.shoppingCart$,
                            builder: (context, shoppingCartSnapshot){
                              return InkWell(
                                child: Icon(Icons.add_shopping_cart_outlined, color: Colors.white, size: 20.0,),
                                onTap: (){
                                  if(shoppingCartSnapshot.hasData && shoppingCartSnapshot.data.length > 0){
                                    for(int i = 0; i < shoppingCartSnapshot.data.length; i++){
                                      if(shoppingCartSnapshot.data[i].productId == productList[index].productId){
                                        itemExist = true;
                                        _shoppingCartBloc.updateShoppingCart(shoppingCartSnapshot.data[i], shoppingCartSnapshot.data[i].quantity + 1);
                                        break;
                                      }
                                    }
                                  }
                                  if(!itemExist){
                                    _productsBloc.insertToShoppingCart(productList[index]);
                                  }
                                  _shoppingCartBloc.getAllShoppingCartItems();
                                  _confirmationDialog('Added to cart');
                                },
                              );
                            },
                          ),
                          InkWell(
                            child: Icon(Icons.playlist_add_outlined, color: Colors.white, size: 20.0,),
                            onTap: (){
                              _productsBloc.insertToWishlist(productList[index]);
                              _confirmationDialog('Added to wishlist');
                            },
                          )
                        ],
                      )
                  ),
                )
              ],
            );
          }),
    );
  }

  Future<void> _confirmationDialog(String text) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          //title: Text('AlertDialog Title'),
          content: Text(text),
          actions: <Widget>[
            TextButton(
              child: Text('OK'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}