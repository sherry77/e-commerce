import 'dart:io';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';

final String tableWishlist = 'wishlist';
final String columnWishlistId = '_id';
final String columnProductId = 'productId';
final String columnProductImageUrl = 'productImageUrl';
final String columnProductName = 'productName';
final String columnProductType = 'productType';
final String columnProductPrice = 'productPrice';

class Wishlist {
  int wishlistId;
  int productId;
  String productImageUrl;
  String productName;
  String productType;
  double productPrice;

  Wishlist();

  Wishlist.fromMap(Map<String, dynamic> map){
    wishlistId = map[columnWishlistId];
    productId = map[columnProductId];
    productImageUrl = map[columnProductImageUrl];
    productName = map[columnProductName];
    productType = map[columnProductType];
    productPrice = map[columnProductPrice];
  }

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      columnProductId: productId,
      columnProductImageUrl: productImageUrl,
      columnProductName: productName,
      columnProductType: productType,
      columnProductPrice: productPrice,
    };
    if(wishlistId != null){
      map[columnWishlistId] = wishlistId;
    }
    return map;
  }
}

class WishlistDatabaseHelper {
  static final _databaseName = "WishlistDatabase.db";
  static final _databaseVersion = 1;

  WishlistDatabaseHelper._privateConstructor();
  static final WishlistDatabaseHelper instance = WishlistDatabaseHelper._privateConstructor();

  static Database _database;
  Future<Database> get database async {
    if(_database != null){
      return _database;
    }
    _database = await _initDatabase();
    return _database;
  }

  _initDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);
    return await openDatabase(path, version: _databaseVersion, onCreate: _onCreate);
  }

  Future _onCreate(Database db, int version) async {
    await db.execute('''
      CREATE TABLE $tableWishlist (
        $columnWishlistId INTEGER PRIMARY KEY,
        $columnProductId INTEGER NOT NULL,
        $columnProductImageUrl TEXT,
        $columnProductName TEXT NOT NULL,
        $columnProductType TEXT NOT NULL,
        $columnProductPrice DOUBLE NOT NULL
      )
    ''');
  }

  Future<int> insert(Wishlist wishlist) async{
    Database db = await database;
    int id = await db.insert(tableWishlist, wishlist.toMap());
    return id;
  }

  Future<List<Wishlist>> getAllWishlistItems() async {
    Database db = await database;
    List<Map> maps = await db.query(tableWishlist);
    if(maps.length > 0) {
      List<Wishlist> wishlistItems = [];
      maps.forEach((map) => wishlistItems.add(Wishlist.fromMap(map)));
      return wishlistItems;
    }
    return null;
  }

  Future<int> deleteWishlistItem(int id) async {
    Database db = await database;
    return await db.delete(tableWishlist, where: '$columnWishlistId = ?', whereArgs: [id]);
  }
}