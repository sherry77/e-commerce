import 'dart:io';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';

final String tableProducts = 'products';
final String columnId = '_id';
final String columnProductId = "productId";
final String columnProductImageUrl = 'productImageUrl';
final String columnProductName = 'productName';
final String columnProductType = 'productType';
final String columnProductPrice = 'productPrice';
final String columnProductQuantity = 'productQuantity';

class Product {
  int id;
  int productId;
  String productImageUrl;
  String productName;
  String productType;
  double productPrice;
  int productQuantity;

  Product();

  Product.fromMap(Map<String, dynamic> map){
    id = map[columnId];
    productId = map[columnProductId];
    productImageUrl = map[columnProductImageUrl];
    productName = map[columnProductName];
    productType = map[columnProductType];
    productPrice = map[columnProductPrice];
    productQuantity = map[columnProductQuantity];
  }

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      columnId: id,
      columnProductId: productId,
      columnProductImageUrl: productImageUrl,
      columnProductName: productName,
      columnProductType: productType,
      columnProductPrice: productPrice,
      columnProductQuantity: productQuantity
    };
    if(productId != null){
      map[columnId] = id;
    }
    return map;
  }
}

class ProductDatabaseHelper {
  static final _databaseName = "ProductDatabase.db";
  static final _databaseVersion = 1;

  ProductDatabaseHelper._privateConstructor();
  static final ProductDatabaseHelper instance = ProductDatabaseHelper._privateConstructor();

  static Database _database;
  Future<Database> get database async {
    if(_database != null){
      return _database;
    }
    _database = await _initDatabase();
    return _database;
  }

  _initDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);
    return await openDatabase(path, version: _databaseVersion, onCreate: _onCreate);
  }

  Future _onCreate(Database db, int version) async {
    await db.execute('''
      CREATE TABLE $tableProducts (
        $columnId INTEGER PRIMARY KEY,
        $columnProductId INTEGER NOT NULL,
        $columnProductImageUrl TEXT,
        $columnProductName TEXT NOT NULL,
        $columnProductType TEXT NOT NULL,
        $columnProductPrice DOUBLE NOT NULL,
        $columnProductQuantity INTEGER NOT NULL
      )
    ''');
  }

  Future<int> insert(Product product) async{
    Database db = await database;
    int id = await db.insert(tableProducts, product.toMap());
    return id;
  }

  Future<List<Product>> getAllProducts() async {
    Database db = await database;
    List<Map> maps = await db.query(tableProducts);
    if(maps.length > 0) {
      List<Product> productItems = [];
      maps.forEach((map) =>productItems.add(Product.fromMap(map)));
      return productItems;
    }
    return null;
  }

  Future<List<Product>> getProducts(String type) async {
    Database db = await database;
    List<Map> maps = await db.query(tableProducts,
        columns: [columnId, columnProductId, columnProductImageUrl, columnProductName, columnProductType, columnProductPrice, columnProductQuantity],
        where: '$columnProductType = ?',
        whereArgs: [type]);
    if (maps.length > 0) {
      List<Product> productItems = [];
      maps.forEach((map) =>productItems.add(Product.fromMap(map)));
      return productItems;
    }
    return null;
  }

  Future<int> deleteProductItem(int id) async {
    Database db = await database;
    return await db.delete(tableProducts, where: '$columnId = ?', whereArgs: [id]);
  }
}