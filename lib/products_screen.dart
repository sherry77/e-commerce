import 'package:e_commerce/shopping_cart_bloc.dart';
import 'package:flutter/material.dart';
import 'package:e_commerce/products_bloc.dart';
import 'package:e_commerce/shopping_cart_screen.dart';
import 'package:e_commerce/widget/menu_display.dart';
import 'package:e_commerce/shopping_cart_database_helper.dart';
import 'package:e_commerce/product_database_helper.dart';
import 'package:e_commerce/all_products.dart';
import 'package:e_commerce/food_products.dart';
import 'package:e_commerce/drinks_product.dart';
import 'package:e_commerce/wishlist_bloc.dart';
import 'package:e_commerce/wishlist_database_helper.dart';

class ProductsScreen extends StatefulWidget {

  @override
  _ProductsScreenState createState() => _ProductsScreenState();
}

class _ProductsScreenState extends State<ProductsScreen> {

  final _productsBloc = ProductsBloc();
  final _shoppingCartBloc = ShoppingCartBloc();
  final _wishlistBloc = WishlistBloc();

  bool cartExist = false;
  bool wishlistExist = false;

  //int radioValue = 0;

  @override
  void initState(){
    super.initState();
    _productsBloc.createProducts();
    _productsBloc.getAllProducts();
    _productsBloc.getFoodProduct();
    _productsBloc.getDrinksProduct();
    _shoppingCartBloc.getAllShoppingCartItems();
    _wishlistBloc.getAllWishlistItems();
  }

  @override
  void dispose(){
    super.dispose();
    _productsBloc.dispose();
    _shoppingCartBloc.dispose();
    _wishlistBloc.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Products"),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.shopping_cart,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => ShoppingCartScreen()),
              );
            },
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            StreamBuilder<int>(
                initialData: 0,
                stream: _productsBloc.productType$,
                builder: (context, typeSnapshot){
                  /*if(typeSnapshot.data == 1){
                  _productsBloc.getFoodProduct();
                } else if(typeSnapshot == 2){
                  _productsBloc.getDrinksProduct();
                } else{
                  _productsBloc.getAllProducts();
                }*/
                  return Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      new Radio(
                        value: 0,
                        groupValue: typeSnapshot.data,
                        onChanged: (val){
                          _productsBloc.productTypeChanged(val);
                        },
                      ),
                      new Text(
                        'All',
                        style: new TextStyle(fontSize: 16.0),
                      ),
                      new Radio(
                        value: 1,
                        groupValue: typeSnapshot.data,
                        onChanged: (val){
                          _productsBloc.productTypeChanged(val);
                        },
                      ),
                      new Text(
                        'Food',
                        style: new TextStyle(fontSize: 16.0),
                      ),
                      new Radio(
                        value: 2,
                        groupValue: typeSnapshot.data,
                        onChanged: (val){
                          _productsBloc.productTypeChanged(val);
                        },
                      ),
                      new Text(
                        'Drinks',
                        style: new TextStyle(
                          fontSize: 16.0,
                        ),
                      ),
                    ],
                  );
                }
            ),
            StreamBuilder<int>(
                stream: _productsBloc.productType$,
                builder: (context, typeSnapshot){
                  /*return StreamBuilder<List<Product>>(
                  stream: _productsBloc.products$,
                  builder: (context, productsSnapshot){

                    if(!productsSnapshot.hasData){
                      return Center(
                        child: CircularProgressIndicator(),
                      );
                    }

                    return Container(
                      margin: EdgeInsets.all(20.0),
                      child: GridView.builder(
                          scrollDirection: Axis.vertical,
                          shrinkWrap: true,
                          physics: ScrollPhysics(),
                          gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                              maxCrossAxisExtent: 200,
                              childAspectRatio: 3 / 2,
                              crossAxisSpacing: 20,
                              mainAxisSpacing: 20),
                          itemCount: productsSnapshot.data.length,
                          itemBuilder: (BuildContext ctx, index) {
                            return Stack(
                              children: [
                                Container(
                                  alignment: Alignment.center,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                      Icon(Icons.shopping_cart),
                                      Text(productsSnapshot.data[index].productName),
                                      Text("RM${productsSnapshot.data[index].productPrice}")
                                    ],
                                  ),
                                  decoration: BoxDecoration(
                                      color: Colors.blue,
                                      borderRadius: BorderRadius.circular(15)),
                                ),
                                Container(
                                  margin: EdgeInsets.all(10.0),
                                  child: Align(
                                      alignment: Alignment.bottomRight,
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.end,
                                        crossAxisAlignment: CrossAxisAlignment.end,
                                        children: [
                                          StreamBuilder<List<ShoppingCart>>(
                                            stream: _shoppingCartBloc.shoppingCart$,
                                            builder: (context, shoppingCartSnapshot){
                                              return InkWell(
                                                child: Icon(Icons.add_shopping_cart_outlined, color: Colors.white, size: 20.0,),
                                                onTap: (){
                                                  if(shoppingCartSnapshot.hasData && shoppingCartSnapshot.data.length > 0){
                                                    for(int i = 0; i < shoppingCartSnapshot.data.length; i++){
                                                      if(shoppingCartSnapshot.data[i].productId == productsSnapshot.data[index].productId){
                                                        itemExist = true;
                                                        _shoppingCartBloc.updateShoppingCart(shoppingCartSnapshot.data[i], shoppingCartSnapshot.data[i].quantity + 1);
                                                        break;
                                                      }
                                                    }
                                                  }
                                                  if(!itemExist){
                                                    _productsBloc.insertToShoppingCart(productsSnapshot.data[index]);
                                                  }
                                                  _shoppingCartBloc.getAllShoppingCartItems();
                                                  _confirmationDialog('Added to cart');
                                                },
                                              );
                                            },
                                          ),
                                          InkWell(
                                            child: Icon(Icons.playlist_add_outlined, color: Colors.white, size: 20.0,),
                                            onTap: (){
                                              _productsBloc.insertToWishlist(productsSnapshot.data[index]);
                                              _confirmationDialog('Added to wishlist');
                                            },
                                          )
                                        ],
                                      )
                                  ),
                                )
                              ],
                            );
                          }),
                    );
                  },
                );*/

                  if(typeSnapshot.data == 1){
                    return _buildAllFood();
                  } else if(typeSnapshot.data == 2){
                    return _buildAllDrinks();
                  } else {
                    return _buildAllProducts();
                  }
                }
            )
          ],
        ),
      ),
      drawer: MenuDisplay(ScreenTitle.products),
    );
  }

  Widget _buildAllProducts(){
    return StreamBuilder<List<Product>>(
        stream: _productsBloc.products$,
        builder: (context, productSnapshot){
          if(!productSnapshot.hasData){
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          List<Product> productList = productSnapshot.data;
          return AllProductsScreen(productList);
          /*return Container(
          margin: EdgeInsets.all(20.0),
          child: GridView.builder(
              scrollDirection: Axis.vertical,
              shrinkWrap: true,
              physics: ScrollPhysics(),
              gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                  maxCrossAxisExtent: 200,
                  childAspectRatio: 3 / 2,
                  crossAxisSpacing: 20,
                  mainAxisSpacing: 20),
              itemCount: productList.length,
              itemBuilder: (BuildContext ctx, index) {
                return Stack(
                  children: [
                    Container(
                      alignment: Alignment.center,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Icon(Icons.shopping_cart),
                          Text(productList[index].productName),
                          Text("RM${productList[index].productPrice}")
                        ],
                      ),
                      decoration: BoxDecoration(
                          color: Colors.blue,
                          borderRadius: BorderRadius.circular(15)),
                    ),
                    Container(
                      margin: EdgeInsets.all(10.0),
                      child: Align(
                          alignment: Alignment.bottomRight,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              StreamBuilder<List<ShoppingCart>>(
                                stream: _shoppingCartBloc.shoppingCart$,
                                builder: (context, shoppingCartSnapshot){
                                  return InkWell(
                                    child: Icon(Icons.add_shopping_cart_outlined, color: Colors.white, size: 20.0,),
                                    onTap: (){
                                      if(shoppingCartSnapshot.hasData && shoppingCartSnapshot.data.length > 0){
                                        for(int i = 0; i < shoppingCartSnapshot.data.length; i++){
                                          if(shoppingCartSnapshot.data[i].productId == productList[index].productId){
                                            itemExist = true;
                                            _shoppingCartBloc.updateShoppingCart(shoppingCartSnapshot.data[i], shoppingCartSnapshot.data[i].quantity + 1);
                                            break;
                                          }
                                        }
                                      }
                                      if(!itemExist){
                                        _productsBloc.insertToShoppingCart(productList[index]);
                                      }
                                      _shoppingCartBloc.getAllShoppingCartItems();
                                      _confirmationDialog('Added to cart');
                                    },
                                  );
                                },
                              ),
                              InkWell(
                                child: Icon(Icons.playlist_add_outlined, color: Colors.white, size: 20.0,),
                                onTap: (){
                                  _productsBloc.insertToWishlist(productList[index]);
                                  _confirmationDialog('Added to wishlist');
                                },
                              )
                            ],
                          )
                      ),
                    )
                  ],
                );
              }),
        );*/
        }
    );
  }

  Widget _buildAllFood(){
    return StreamBuilder<List<Product>>(
        stream: _productsBloc.foodProduct$,
        builder: (context, productSnapshot){
          if(!productSnapshot.hasData){
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          List<Product> productList = productSnapshot.data;
          return FoodProductsScreen(productList);
          /*return Container(
            margin: EdgeInsets.all(20.0),
            child: GridView.builder(
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                physics: ScrollPhysics(),
                gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                    maxCrossAxisExtent: 200,
                    childAspectRatio: 3 / 2,
                    crossAxisSpacing: 20,
                    mainAxisSpacing: 20),
                itemCount: productList.length,
                itemBuilder: (BuildContext ctx, index) {
                  return Stack(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Icon(Icons.shopping_cart),
                            Text(productList[index].productName),
                            Text("RM${productList[index].productPrice}")
                          ],
                        ),
                        decoration: BoxDecoration(
                            color: Colors.blue,
                            borderRadius: BorderRadius.circular(15)),
                      ),
                      Container(
                        margin: EdgeInsets.all(10.0),
                        child: Align(
                            alignment: Alignment.bottomRight,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                StreamBuilder<List<ShoppingCart>>(
                                  stream: _shoppingCartBloc.shoppingCart$,
                                  builder: (context, shoppingCartSnapshot){
                                    return InkWell(
                                      child: Icon(Icons.add_shopping_cart_outlined, color: Colors.white, size: 20.0,),
                                      onTap: (){
                                        if(shoppingCartSnapshot.hasData && shoppingCartSnapshot.data.length > 0){
                                          for(int i = 0; i < shoppingCartSnapshot.data.length; i++){
                                            if(shoppingCartSnapshot.data[i].productId == productList[index].productId){
                                              itemExist = true;
                                              _shoppingCartBloc.updateShoppingCart(shoppingCartSnapshot.data[i], shoppingCartSnapshot.data[i].quantity + 1);
                                              break;
                                            }
                                          }
                                        }
                                        if(!itemExist){
                                          _productsBloc.insertToShoppingCart(productList[index]);
                                        }
                                        _shoppingCartBloc.getAllShoppingCartItems();
                                        _confirmationDialog('Added to cart');
                                      },
                                    );
                                  },
                                ),
                                InkWell(
                                  child: Icon(Icons.playlist_add_outlined, color: Colors.white, size: 20.0,),
                                  onTap: (){
                                    _productsBloc.insertToWishlist(productList[index]);
                                    _confirmationDialog('Added to wishlist');
                                  },
                                )
                              ],
                            )
                        ),
                      )
                    ],
                  );
                }),
          );*/
        }
    );
  }

  Widget _buildAllDrinks(){
    return StreamBuilder<List<Product>>(
        stream: _productsBloc.drinkProduct$,
        builder: (context, productSnapshot){
          if(!productSnapshot.hasData){
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          List<Product> productList = productSnapshot.data;
          return DrinksProductsScreen(productList);
          /*return Container(
            margin: EdgeInsets.all(20.0),
            child: GridView.builder(
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                physics: ScrollPhysics(),
                gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                    maxCrossAxisExtent: 200,
                    childAspectRatio: 3 / 2,
                    crossAxisSpacing: 20,
                    mainAxisSpacing: 20),
                itemCount: productList.length,
                itemBuilder: (BuildContext ctx, index) {
                  return Stack(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Icon(Icons.shopping_cart),
                            Text(productList[index].productName),
                            Text("RM${productList[index].productPrice}")
                          ],
                        ),
                        decoration: BoxDecoration(
                            color: Colors.blue,
                            borderRadius: BorderRadius.circular(15)),
                      ),
                      Container(
                        margin: EdgeInsets.all(10.0),
                        child: Align(
                            alignment: Alignment.bottomRight,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                StreamBuilder<List<ShoppingCart>>(
                                  stream: _shoppingCartBloc.shoppingCart$,
                                  builder: (context, shoppingCartSnapshot){
                                    return InkWell(
                                      child: Icon(Icons.add_shopping_cart_outlined, color: Colors.white, size: 20.0,),
                                      onTap: (){
                                        if(shoppingCartSnapshot.hasData && shoppingCartSnapshot.data.length > 0){
                                          for(int i = 0; i < shoppingCartSnapshot.data.length; i++){
                                            if(shoppingCartSnapshot.data[i].productId == productList[index].productId){
                                              itemExist = true;
                                              _shoppingCartBloc.updateShoppingCart(shoppingCartSnapshot.data[i], shoppingCartSnapshot.data[i].quantity + 1);
                                              break;
                                            }
                                          }
                                        }
                                        if(!itemExist){
                                          _productsBloc.insertToShoppingCart(productList[index]);
                                        }
                                        _shoppingCartBloc.getAllShoppingCartItems();
                                        _confirmationDialog('Added to cart');
                                      },
                                    );
                                  },
                                ),
                                InkWell(
                                  child: Icon(Icons.playlist_add_outlined, color: Colors.white, size: 20.0,),
                                  onTap: (){
                                    _productsBloc.insertToWishlist(productList[index]);
                                    _confirmationDialog('Added to wishlist');
                                  },
                                )
                              ],
                            )
                        ),
                      )
                    ],
                  );
                }),
          );*/
        }
    );
  }

  Widget _buildProductList(List<Product> productList){

    return Container(
      margin: EdgeInsets.all(20.0),
      child: GridView.builder(
          scrollDirection: Axis.vertical,
          shrinkWrap: true,
          physics: ScrollPhysics(),
          gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
              maxCrossAxisExtent: 200,
              childAspectRatio: 3 / 2,
              crossAxisSpacing: 20,
              mainAxisSpacing: 20),
          itemCount: productList.length,
          itemBuilder: (BuildContext ctx, index) {
            return Stack(
              children: [
                Container(
                  alignment: Alignment.center,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(Icons.shopping_cart),
                      Text(productList[index].productName),
                      Text("RM${productList[index].productPrice}")
                    ],
                  ),
                  decoration: BoxDecoration(
                      color: Colors.blue,
                      borderRadius: BorderRadius.circular(15)),
                ),
                Container(
                  margin: EdgeInsets.all(10.0),
                  child: Align(
                      alignment: Alignment.bottomRight,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          StreamBuilder<List<ShoppingCart>>(
                            stream: _shoppingCartBloc.shoppingCart$,
                            builder: (context, shoppingCartSnapshot){
                              return InkWell(
                                child: Icon(Icons.add_shopping_cart_outlined, color: Colors.white, size: 20.0,),
                                onTap: (){
                                  /*if(shoppingCartSnapshot.hasData && shoppingCartSnapshot.data.length > 0){
                                    for(int i = 0; i < shoppingCartSnapshot.data.length; i++){
                                      if(shoppingCartSnapshot.data[i].productId == productList[index].productId){
                                        cartExist = true;
                                        _shoppingCartBloc.updateShoppingCart(shoppingCartSnapshot.data[i], shoppingCartSnapshot.data[i].quantity + 1);
                                        break;
                                      }
                                    }
                                  }
                                  if(!cartExist){
                                    _productsBloc.insertToShoppingCart(productList[index]);
                                  }*/
                                  _productsBloc.insertToShoppingCart(productList[index]);
                                  _shoppingCartBloc.getAllShoppingCartItems();
                                  _confirmationDialog('Added to cart');
                                },
                              );
                            },
                          ),
                          StreamBuilder<List<Wishlist>>(
                            stream: _wishlistBloc.wishlist$,
                            builder: (context, wishlistSnapshot){
                              return InkWell(
                                child: Icon(Icons.playlist_add_outlined, color: Colors.white, size: 20.0,),
                                onTap: (){

                                  _productsBloc.insertToWishlist(productList[index]);
                                  _confirmationDialog('Added to wishlist');
                                  _wishlistBloc.getAllWishlistItems();
                                },
                              );
                            },
                          )
                        ],
                      )
                  ),
                )
              ],
            );
          }),
    );
  }

  Future<void> _confirmationDialog(String text) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          //title: Text('AlertDialog Title'),
          content: Text(text),
          actions: <Widget>[
            TextButton(
              child: Text('OK'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}