import 'package:flutter/material.dart';
import 'package:e_commerce/widget/menu_display.dart';
import 'package:e_commerce/wishlist_database_helper.dart';
import 'package:e_commerce/wishlist_bloc.dart';
import 'package:e_commerce/shopping_cart_screen.dart';

class WishListScreen extends StatefulWidget {

  @override
  _WishListScreenState createState() => _WishListScreenState();
}

class _WishListScreenState extends State<WishListScreen> {

  final _wishlistBloc = WishlistBloc();

  @override
  void initState(){
    super.initState();
    _wishlistBloc.getAllWishlistItems();
  }

  @override
  void dispose(){
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("WishList"),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.shopping_cart,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => ShoppingCartScreen()),
              );
            },
          )
        ],
      ),
      body: StreamBuilder<List<Wishlist>>(
        stream: _wishlistBloc.wishlist$,
        builder: (context, wishlistSnapshot){
          return wishlistSnapshot.hasData && wishlistSnapshot.data.length > 0 ? Container(
            margin: EdgeInsets.all(20.0),
            child: GridView.builder(
                gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                    maxCrossAxisExtent: 200,
                    childAspectRatio: 3 / 2,
                    crossAxisSpacing: 20,
                    mainAxisSpacing: 20),
                itemCount: wishlistSnapshot.data.length,
                itemBuilder: (BuildContext ctx, index) {
                  return Container(
                    alignment: Alignment.center,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Icon(Icons.playlist_add_sharp),
                        Text(wishlistSnapshot.data[index].productName),
                        Text("RM${wishlistSnapshot.data[index].productPrice}")
                      ],
                    ),
                    decoration: BoxDecoration(
                        color: Colors.blue,
                        borderRadius: BorderRadius.circular(15)),
                  );
                }),
          ) : Center(child: Text("Wishlist is empty"));
        },
      ),
      drawer: MenuDisplay(ScreenTitle.wishlist),
    );
  }
}