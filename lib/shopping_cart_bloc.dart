import 'package:rxdart/rxdart.dart';
import 'package:e_commerce/shopping_cart_database_helper.dart';
import 'package:e_commerce/order_history_database_helper.dart';

class ShoppingCartBloc {

  final _shoppingCartSubject = BehaviorSubject<List<ShoppingCart>>();
  BehaviorSubject<List<ShoppingCart>> get shoppingCart$ => _shoppingCartSubject.stream;

  final _checkedShoppingCartItemsSubject = BehaviorSubject<List<bool>>();
  BehaviorSubject<List<bool>> get checkedShoppingCartItems$ => _checkedShoppingCartItemsSubject.stream;

  final _selectedItemsSubject = BehaviorSubject<List<ShoppingCart>>();
  BehaviorSubject<List<ShoppingCart>> get selectedItems$ => _selectedItemsSubject.stream;

  final _totalPriceSubject = BehaviorSubject<double>();
  BehaviorSubject<double> get totalPrice$ => _totalPriceSubject.stream;

  final _totalToPaySubject = BehaviorSubject<double>();
  BehaviorSubject<double> get totalToPay$ => _totalToPaySubject.stream;

  final _productQuantityListSubject = BehaviorSubject<List<int>>();
  BehaviorSubject<List<int>> get productQuantity$ => _productQuantityListSubject.stream;

  final _orderHistoryListSubject = BehaviorSubject<List<OrderHistory>>();
  BehaviorSubject<List<OrderHistory>> get orderHistoryList$ => _orderHistoryListSubject.stream;

  List<ShoppingCart> selectedItemList = List<ShoppingCart>();

  List<bool> checked = List<bool>();

  List<OrderHistory> orderHistoryList = List<OrderHistory>();

  getAllShoppingCartItems() async{
    List<ShoppingCart> shoppingCartList = List<ShoppingCart>();
    ShoppingCartDatabaseHelper helper = ShoppingCartDatabaseHelper.instance;
    final cartItems = await helper.getAllShoppingCartItems();
    if(cartItems != null) {
      cartItems.forEach((cartItems) {
        shoppingCartList.add(cartItems);
      });
      _shoppingCartSubject.sink.add(shoppingCartList);
    }
  }

  /*getAllShoppingCartItems() async{
    bool repeated = false;
    List<ShoppingCart> shoppingCartList = List<ShoppingCart>();
    ShoppingCartDatabaseHelper helper = ShoppingCartDatabaseHelper.instance;
    final shoppingCartItems = await helper.getAllShoppingCartItems();
    if(shoppingCartItems != null) {
      /*shoppingCartItems.forEach((shoppingCartItems) {
        shoppingCartList.add(shoppingCartItems);
      });
      _shoppingCartSubject.sink.add(shoppingCartList);*/
      for(int i = 0; i < shoppingCartItems.length; i++){
        if( i > 0){
          for(int j = 0; j < i; j++){
            if(shoppingCartItems[j].shoppingCartId == shoppingCartItems[i]){
              repeated = true;
              updateShoppingCart(shoppingCartItems[i], shoppingCartItems[i].quantity + 1);
              break;
            }
          }
          if(!repeated){
            shoppingCartList.add(shoppingCartItems[i]);
          } /*else{
            updateShoppingCart(shoppingCartItems[i], shoppingCartItems[i].quantity + 1);
          }*/
        }
      }
      _shoppingCartSubject.sink.add(shoppingCartList);
    }
  }*/

  insertToOrderHistory(List<ShoppingCart> cartList) async{
    OrderHistoryDatabaseHelper helper = OrderHistoryDatabaseHelper.instance;
    List<OrderHistory> orderHistoryItems = await helper.getAllOrderHistoryItems();
    bool orderHistoryExist = false;
    if(cartList != null && cartList.length > 0){
      for(int i = 0; i < cartList.length; i++){
        if(orderHistoryItems != null && orderHistoryItems.length > 0){
          for(int j = 0; j < orderHistoryItems.length; j++){
            if(cartList[i].productId == orderHistoryItems[j].productId){
              orderHistoryExist = true;
              updateOrderHistory(orderHistoryItems[j], orderHistoryItems[j].quantity + cartList[i].quantity);
              break;
            }
          }
        }
        if(!orderHistoryExist){
          OrderHistory orderHistory = OrderHistory();
          orderHistory.productId = cartList[i].productId;
          orderHistory.productImageUrl = cartList[i].productImageUrl;
          orderHistory.productName = cartList[i].productName;
          orderHistory.productType = cartList[i].productType;
          orderHistory.productPrice = cartList[i].productPrice;
          orderHistory.quantity = cartList[i].quantity;

          if(_shoppingCartSubject.value.length > 0){
            for(int cartItem = 0; cartItem < _shoppingCartSubject.value.length; cartItem++){
              if(_shoppingCartSubject.value[cartItem].shoppingCartId == cartList[i].productId){
                orderHistory.quantity = _shoppingCartSubject.value[cartItem].quantity;
              }
            }
          }

          orderHistoryList.add(orderHistory);
          _orderHistoryListSubject.sink.add(orderHistoryList);

          await helper.insert(orderHistory);
        }
      }
    }

  }

  updateOrderHistory(OrderHistory item, int quantity) async{
    print("Update quantity: ${quantity}");
    OrderHistory orderHistory = OrderHistory();
    orderHistory.orderHistoryId = item.orderHistoryId;
    orderHistory.productId = item.productId;
    orderHistory.productImageUrl = item.productImageUrl;
    orderHistory.productName = item.productName;
    orderHistory.productType = item.productType;
    orderHistory.productPrice = item.productPrice;
    orderHistory.quantity = quantity;
    final helper = OrderHistoryDatabaseHelper.instance;
    await helper.update(orderHistory);
  }

  updateShoppingCart(ShoppingCart item, int quantity) async{
    print("Update quantity: ${quantity}");
    ShoppingCart cart = ShoppingCart();
    cart.shoppingCartId = item.shoppingCartId;
    cart.productId = item.productId;
    cart.productImageUrl = item.productImageUrl;
    cart.productName = item.productName;
    cart.productType = item.productType;
    cart.productPrice = item.productPrice;
    cart.quantity = quantity;
    final helper = ShoppingCartDatabaseHelper.instance;
    await helper.update(cart);
  }

  removeCartItemsAfterCheckout(List<ShoppingCart> cartList) async{
    final helper = ShoppingCartDatabaseHelper.instance;
    if(cartList != null && cartList.length > 0){
      for(int i = 0; i < cartList.length; i++){
        await helper.deleteShoppingCartItem(cartList[i].shoppingCartId);
      }
    }
  }

  deleteShoppingCart(List<ShoppingCart> cartList, ShoppingCart cartToBeRemoved) async{
    final helper = ShoppingCartDatabaseHelper.instance;
    await helper.deleteShoppingCartItem(cartToBeRemoved.shoppingCartId);
    removeShoppingCartFromList(cartList, cartToBeRemoved);
  }

  removeShoppingCartFromList(List<ShoppingCart> cartList, ShoppingCart cartToBeRemoved){
    cartList.remove(cartToBeRemoved);
    _shoppingCartSubject.sink.add(cartList);
  }

  addToSelectedItems(ShoppingCart cart, int count){
    selectedItemList.add(cart);
    _selectedItemsSubject.sink.add(selectedItemList);
    double price = _totalPriceSubject.value + (cart.productPrice * _productQuantityListSubject.value[count]);
    _totalPriceSubject.sink.add(price);
    if(checked != null && checked.length > 0){
      checked[count] = true;
      _checkedShoppingCartItemsSubject.sink.add(checked);
    }
  }

  removeFromSelectedItems(ShoppingCart cart, int count){
    double price = _totalPriceSubject.value - (cart.productPrice * _productQuantityListSubject.value[count]);
    _totalPriceSubject.sink.add(price);
    selectedItemList.remove(cart);
    _selectedItemsSubject.sink.add(selectedItemList);
    if(checked != null && checked.length > 0){
      checked[count] = false;
      _checkedShoppingCartItemsSubject.sink.add(checked);
    }
  }

  addAllToSelectedItems(List<ShoppingCart> cartList){
    double price = 0;
    _totalPriceSubject.sink.add(price);
    if(cartList != null && cartList.length > 0){
      selectedItemList = new List<ShoppingCart>();
      for(int i = 0; i < cartList.length; i++){
        selectedItemList.add(cartList[i]);
        int qty = cartList[i].quantity;
        for(int j = 0; j < _shoppingCartSubject.value.length; j++){
          if(_shoppingCartSubject.value[j].shoppingCartId == cartList[i].shoppingCartId){
            qty = _productQuantityListSubject.value[j];
            price += (cartList[i].productPrice * qty);
          }
        }
      }
      _selectedItemsSubject.sink.add(selectedItemList);
      _totalPriceSubject.sink.add(price);
    }
    if(checked != null && checked.length > 0){
      for(int i = 0; i < checked.length; i++){
        checked[i] = true;
        _checkedShoppingCartItemsSubject.sink.add(checked);
      }
    }
  }

  removeAllFromSelectedItems(){
    selectedItemList = new List<ShoppingCart>();
    if(checked != null && checked.length > 0){
      for(int i = 0; i < checked.length; i++){
        checked[i] = false;
        _checkedShoppingCartItemsSubject.sink.add(checked);
      }
    }
    _totalPriceSubject.sink.add(0);
  }

  initializeCheckedList(int total){
    if(checked == null) checked = List<bool>();
    if(total > 0){
      for(int i = 0; i < total; i ++){
        checked.insert(i, false);
      }
      _checkedShoppingCartItemsSubject.sink.add(checked);
    }
  }

  addToTotalPrice(double price){
    double addedPrice = _totalPriceSubject.value + price;
    _totalPriceSubject.sink.add(addedPrice);
  }

  reduceTotalPrice(double price){
    double removedPrice = _totalPriceSubject.value - price;
    _totalPriceSubject.sink.add(removedPrice);
  }

  initializeTotalPrice(){
    _totalPriceSubject.sink.add(0);
  }

  initializeProductQuantity(List<ShoppingCart> cartList){
    if(cartList != null && cartList.length > 0){
      List<int> productQuantityList = List<int>();
      for(int i = 0; i < cartList.length; i++){
        productQuantityList.add(cartList[i].quantity);
      }
      _productQuantityListSubject.sink.add(productQuantityList);
    }
  }

  addQuantity(int pos){
    List<int> qty = _productQuantityListSubject.value;
    qty[pos] += 1;
    _productQuantityListSubject.sink.add(qty);
  }

  reduceQuantity(int pos){
    List<int> qty = _productQuantityListSubject.value;
    qty[pos] -= 1;
    _productQuantityListSubject.sink.add(qty);
  }

  calculateTotalToPay(double price){
    _totalToPaySubject.sink.add(price);
  }

  void dispose(){
    _shoppingCartSubject.close();
    _checkedShoppingCartItemsSubject.close();
    _selectedItemsSubject.close();
    _totalPriceSubject.close();
    _totalToPaySubject.close();
    _productQuantityListSubject.close();
  }
}