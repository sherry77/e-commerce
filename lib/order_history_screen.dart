import 'package:flutter/material.dart';
import 'package:e_commerce/order_history_database_helper.dart';
import 'package:e_commerce/order_history_bloc.dart';
import 'package:e_commerce/widget/menu_display.dart';
import 'package:e_commerce/shopping_cart_screen.dart';
import 'package:e_commerce/shopping_cart_database_helper.dart';

class OrderHistoryScreen extends StatefulWidget {

  @override
  _OrderHistoryScreenState createState() => _OrderHistoryScreenState();
}

class _OrderHistoryScreenState extends State<OrderHistoryScreen> {

  final _orderHistoryBloc = OrderHistoryBloc();

  @override
  void initState(){
    super.initState();
    _orderHistoryBloc.getAllOrderHistoryItems();
  }

  @override
  void dispose(){
    super.dispose();
    _orderHistoryBloc.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _orderHistoryBloc.getAllOrderHistoryItems();
    return Scaffold(
        appBar: AppBar(
          title: Text("Order History"),
          actions: <Widget>[
            IconButton(
              icon: Icon(
                Icons.shopping_cart,
                color: Colors.white,
              ),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => ShoppingCartScreen()),
                );
              },
            )
          ],
        ),
        body: StreamBuilder<List<OrderHistory>>(
          stream: _orderHistoryBloc.orderHistory$,
          builder: (context, orderHistorySnapshot){

            _orderHistoryBloc.getAllOrderHistoryItems();

            return orderHistorySnapshot.hasData ? Container(
              margin: EdgeInsets.all(20.0),
              child: GridView.builder(
                  gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                      maxCrossAxisExtent: 200,
                      childAspectRatio: 3 / 2,
                      crossAxisSpacing: 20,
                      mainAxisSpacing: 20),
                  itemCount: orderHistorySnapshot.data.length,
                  itemBuilder: (BuildContext ctx, index) {
                    return Container(
                      alignment: Alignment.center,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Icon(Icons.playlist_add_sharp),
                          Text('${orderHistorySnapshot.data[index].productName}'),
                          Text("RM${orderHistorySnapshot.data[index].productPrice}")
                        ],
                      ),
                      decoration: BoxDecoration(
                          color: Colors.blue,
                          borderRadius: BorderRadius.circular(15)),
                    );
                  }),
            ) : Center(child: Text("Order history is empty"));

            /*if(!orderHistorySnapshot.hasData){
              return Center(child: CircularProgressIndicator());
            }

            _orderHistoryBloc.getAllOrderHistoryItems();

            if(orderHistorySnapshot.data.length == 0){
              return Center(child: Text("Order history is empty"));
            } else{
              return Container(
                margin: EdgeInsets.all(20.0),
                child: GridView.builder(
                    gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                        maxCrossAxisExtent: 200,
                        childAspectRatio: 3 / 2,
                        crossAxisSpacing: 20,
                        mainAxisSpacing: 20),
                    itemCount: orderHistorySnapshot.data.length,
                    itemBuilder: (BuildContext ctx, index) {
                      return Container(
                        alignment: Alignment.center,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Icon(Icons.playlist_add_sharp),
                            Text('${orderHistorySnapshot.data[index].productName} x ${orderHistorySnapshot.data[index].quantity}'),
                            Text("RM${orderHistorySnapshot.data[index].productPrice}")
                          ],
                        ),
                        decoration: BoxDecoration(
                            color: Colors.blue,
                            borderRadius: BorderRadius.circular(15)),
                      );
                    }),
              );
            }*/
            /*return orderHistorySnapshot.hasData && orderHistorySnapshot.data.length > 0 ? Container(
              margin: EdgeInsets.all(20.0),
              child: GridView.builder(
                  gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                      maxCrossAxisExtent: 200,
                      childAspectRatio: 3 / 2,
                      crossAxisSpacing: 20,
                      mainAxisSpacing: 20),
                  itemCount: orderHistorySnapshot.data.length,
                  itemBuilder: (BuildContext ctx, index) {
                    return Container(
                      alignment: Alignment.center,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Icon(Icons.playlist_add_sharp),
                          Text('${orderHistorySnapshot.data[index].productName} x ${orderHistorySnapshot.data[index].quantity}'),
                          Text("RM${orderHistorySnapshot.data[index].productPrice}")
                        ],
                      ),
                      decoration: BoxDecoration(
                          color: Colors.blue,
                          borderRadius: BorderRadius.circular(15)),
                    );
                  }),
            ) : Center(child: Text("Order history is empty"));*/
          },
        ),
        drawer: MenuDisplay(ScreenTitle.orderHistory),
    );
  }
}